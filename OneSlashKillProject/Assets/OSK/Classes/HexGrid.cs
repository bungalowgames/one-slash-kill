﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace OSK
{
	using Gamelogic;
	using Gamelogic.Grids;

	public class HexGrid  : GLMonoBehaviour
	{
		private readonly Vector2 HexDimensions = new Vector2(239, 206);
		
		public SpriteCell cellPrefab;
		public GameObject root;
		private FlatHexGrid<SpriteCell> grid;
		private IMap3D<FlatHexPoint> map;
		private List<FlatHexPoint> linearPoints;

		public void Awake ()
		{
			linearPoints = new List<FlatHexPoint>();
		}

		public void Start()
		{		
			BuildGrid();
		}
		
		public void Update()
		{
			if(Input.GetMouseButtonDown(0))
			{
				Vector3 worldPosition = GridBuilderUtils.ScreenToWorld(root, Input.mousePosition);

				FlatHexPoint hexPoint = map[worldPosition];
				
				if(grid.Contains(hexPoint))
				{
					// highlight selected tile
					//grid[hexPoint].HighlightOn = !grid[hexPoint].HighlightOn;
					
					// highlight neighbors (1 neighbor distance)
					//this.HighlightNeighbors(hexPoint);

					// Linear Highlight
					this.HighlightLinearPoints(hexPoint);
				}
			}		
		}
		
		private void BuildGrid()
		{
			root.transform.DestroyChildren();
			
			grid = FlatHexGrid<SpriteCell>.Hexagon(4);
			
			map = new FlatHexMap(HexDimensions)
				.AnchorCellMiddleCenter()
				.WithWindow(ExampleUtils.ScreenRect)
				.AlignMiddleCenter(grid)
				.To3DXY();	
			
			foreach(FlatHexPoint point in grid)
			{
				SpriteCell cell = Instantiate(cellPrefab);
				Vector3 worldPoint = map[point];
				
				cell.transform.parent = root.transform;
				cell.transform.localScale = Vector3.one;
				cell.transform.localPosition = worldPoint;	
				
				cell.Color = ExampleUtils.Colors[point.GetColor3_7()];
				cell.name = point.ToString();
				
				grid[point] = cell;
			}
		}

		private void HighlightNeighbors (FlatHexPoint center)
		{
			if(grid.Contains(center))
			{
				// highlight selected tile
				//grid[hexPoint].HighlightOn = !grid[hexPoint].HighlightOn;
				
				// highlight neighbors (1 neighbor distance)
				IEnumerable<FlatHexPoint> neighbors = grid.GetAllNeighbors(center);
				
				//IEnumerable<FlatHexPoint> neighbors = grid.GetNeighbors(hexPoint);
				
				//IEnumerable<FlatHexPoint> neighbors = grid.GetNeighborDirections(0);
				//IEnumerable<FlatHexPoint> neighbors = grid.GetNeighborDirections();
				
				//IEnumerable<FlatHexPoint> neighbors = grid.GetPrincipleNeighborDirections();

				foreach (FlatHexPoint neighbor in neighbors)
				{
					grid[neighbor].HighlightOn = !grid[neighbor].HighlightOn;
				}
			}
		}

		private void HighlightLinearPoints (FlatHexPoint target)
		{
			if (grid[target].HighlightOn) { return; }

			grid[target].HighlightOn = true;

			// collect linear points
			linearPoints.Add(target);

			for (int i = 0; i < linearPoints.Count; i++)
			{
				int j = i+1;

				// block out of bounds
				if (j >= linearPoints.Count) { continue; }

				FlatHexPoint headPoint = linearPoints[i];
				FlatHexPoint tailPoint = linearPoints[j];

				List<FlatHexPoint> points =  map.GetLine(headPoint, tailPoint);
				
				foreach (FlatHexPoint point in points)
				{
					grid[point].HighlightOn = true;
				}
			}
		}
	}
}
