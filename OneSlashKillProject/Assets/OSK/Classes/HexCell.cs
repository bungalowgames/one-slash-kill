﻿using UnityEngine;
using System.Collections;

namespace OSK
{
	using Gamelogic;
	using Gamelogic.Grids;

	public enum ECellStatus
	{
		UNSELECTED,
		SELECTED,
	};

	[RequireComponent(typeof(SpriteCell))]
	public class HexCell : MonoBehaviour 
	{
		[SerializeField] private ECellStatus status;

		private SpriteCell spriteCell;

		private void Awake ()
		{
			// cache components
			this.spriteCell = this.GetComponent<SpriteCell>();
		}

		public ECellStatus Status
		{
			get { return status; }
			set { status = value; }
		}

		public void Select ()
		{
			this.Status = ECellStatus.SELECTED;
			this.spriteCell.HighlightOn = true;
		}

		public void Deselect ()
		{
			this.Status = ECellStatus.UNSELECTED;
			this.spriteCell.HighlightOn = false;
		}
	}
}